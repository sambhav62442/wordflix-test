import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
  name: { type: String, required: true },
  email: {
    type: String, required: true, unique: true, index: true, dropDups: true,
  },
  password: { type: String, required: true },
  isAdmin: { type: Boolean, required: true, default: false },
  phone: {type: Number},
  countryCode: {type: String}
}, {
  timestamps: true
});

const userModel = mongoose.model('Users', userSchema);

export default userModel;

import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const paymentSchema = {
  paymentMethod: { type: String },
  paymentResult: {
    payerID: { type: String },
    orderID: { type: String },
    paymentID: { type: String }
  }
};

const orderItemSchema = {
  name: { type: String, required: true },
  price: { type: String, required: true },
  image: {type: String},
  course: {
    type: Schema.Types.ObjectId,
    ref: 'Course',
    required: true
  },
};

const orderSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: 'Users', required: true },
  orderItems: [orderItemSchema],
  payment: paymentSchema,
  itemPrice: { type: Number },
  taxPrice: { type: Number },
  totalPrice: { type: Number },
  isPaid: { type: Boolean, default: false },
  paidAt: { type: Date },
}, {
  timestamps: true
});

const orderModel = mongoose.model("Orders", orderSchema);
export default orderModel;
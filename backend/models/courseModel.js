import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const reviewSchema = new Schema(
  {
    name: { type: String, required: true },
    rating: { type: Number, default: 0 },
    comment: { type: String, required: true },
  },
  {
    timestamps: true,
  }
);

const courseSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String },
  image: { type: String },
  category: { type: String },
  price: { type: String },
  rating: { type: String },
  reviews: [reviewSchema],
  numReviews: {type: String, default: 0},
  videos: [{ type: Schema.Types.ObjectId, ref: 'Videos' }]
}, {
  timestamps: true
});

const courseModel = mongoose.model("Courses", courseSchema);
export default courseModel;
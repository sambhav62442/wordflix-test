import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const subscriptionSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: 'Users', required: true },
  course:  {type: Schema.Types.ObjectId, ref: 'Courses', required: true },
  status: {type: Number, required: true, default: 0},
  expiryDate: {type: Date}
}, {
  timestamps: true
});

const subscriptionModel = mongoose.model("Subscriptions", subscriptionSchema);
export default subscriptionModel;
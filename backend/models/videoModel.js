import mongoose from 'mongoose';

const videoSchema = new mongoose.Schema({
  vimeoid: { type: Number, required: true },
  name: { type: String, required: true },
  description: { type: String },
  image: {type: String},
  duration: { type: Number, default: 0 },
  width: { type: Number },
  height: { type: Number }
}, {
    timestamps: true
});

const videoModel = mongoose.model('Videos', videoSchema);

export default videoModel;

import jwt from 'jsonwebtoken';
import config from './config';
import Subscription from './models/subscriptionModel';
import Course from './models/courseModel';
const getToken = (user) => {
  return jwt.sign(
    {
      _id: user._id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
      phone: user.phone
    },
    config.JWT_SECRET,
    {
      expiresIn: '48h',
    }
  );
};

const isAuth = (req, res, next) => {
  const token = req.headers.authorization;

  if (token) {
    const onlyToken = token.slice(7, token.length);
    jwt.verify(onlyToken, config.JWT_SECRET, (err, decode) => {
      if (err) {
        return res.status(401).send({ message: 'Invalid Token' });
      }
      req.user = decode;
      next();
      return;
    });
  } else {
    return res.status(401).send({ message: 'Token is not supplied.' });
  }
};

const isAdmin = (req, res, next) => {
  console.log(req.user);
  if (req.user && req.user.isAdmin) {
    return next();
  }
  return res.status(401).send({ message: 'Admin Token is not valid.' });
};

const isCourseSubscribed = async (req, res, next) => {
  try {
    var status = false;
    const courses = await Course.find({ videos: { $in: req.params.id } }, { _id: 1 });
    if(courses != null && courses.length > 0) {
      courses.map(async course => {
        const subscription = await Subscription.findOne({ user: req.user._id, course: course._id, status: 1 });
        console.log("subscription", subscription);
        if(subscription && subscription != null) {
          console.log('function is working');
          return next();
        } else {
          return res.status(401).send({ message: 'Admin Token is not valid.' });
        }
      })
    } else {
      return res.status(401).send({ message: 'Admin Token is not valid.' });
    }
  }
  catch(error) {
    console.log(error);
    next(error);
  }
}

export { getToken, isAuth, isAdmin, isCourseSubscribed };

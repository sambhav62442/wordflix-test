import express from 'express';
import Subscription from '../models/subscriptionModel';
import { isAuth, isAdmin } from '../util';

const router = express.Router();

router.get("/", isAuth, async (req, res) => {
  const subscription = await Subscription.find({}).populate('user');
  res.send(subscriptions);
});
router.get("/mine", isAuth, async (req, res) => {
  const subscriptions = await Subscription.find({ user: req.user._id }).populate('course');
  res.send(subscriptions);
});

router.get("/:id", isAuth, async (req, res) => {
  const subscription = await Subscription.findOne({ _id: req.params.id });
  if (subscription) {
    res.send(subscription);
  } else {
    res.status(404).send("Subscription Not Found.")
  }
});

router.get("/course/:courseId", isAuth, async (req, res) => {
  const subscription = await Subscription.findOne({ user: req.user._id, course: req.params.courseId });
  if (subscription) {
    res.send(subscription);
  } else {
    res.status(404).send("Subscription Not Found.")
  }
});

router.delete("/:id", isAuth, isAdmin, async (req, res) => {
  const subscription = await Subscription.findOne({ _id: req.params.id });
  if (subscription) {
    const deletedSubscription = await subscription.remove();
    res.send(deletedSubscription);
  } else {
    res.status(404).send("Subscription Not Found.")
  }
});

router.post("/", isAuth, async (req, res) => {
  const newSubscription = new Subscription({
    user: req.user._id,
    course: req.body.course,
    status: req.body.status,
    expiryDate: req.body.expiryDate
  });
  const newSubscriptionCreated = await newSubscription.save();
  res.status(201).send({ message: "New Subscription Created", data: newSubscriptionCreated });
});

router.put("/:id", isAuth, async (req, res) => {
  const subscription = await Subscription.findById(req.params.id);
  if (subscription) {
    subscription.user = req.body.user;
    subscription.course = req.body.course;
    subscription.status = req.body.status;
    subscription.expiryDate = req.body.expiryDate
    const updatedSubscription = await subscription.save();
    res.send({ message: 'Subscription updated.', subscription: updatedSubscription });
  } else {
    res.status(404).send({ message: 'Subscription not found.' })
  }
});

export default router;
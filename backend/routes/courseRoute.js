import express from 'express';
import Course from '../models/courseModel';
import { isAuth, isAdmin, isCourseSubscribed } from '../util';

const router = express.Router();

router.get('/', async (req, res) => {
  const category = req.query.category ? { category: req.query.category } : {};
  const searchKeyword = req.query.searchKeyword
    ? {
        name: {
          $regex: req.query.searchKeyword,
          $options: 'i',
        },
      }
    : {};
  const sortOrder = req.query.sortOrder
    ? req.query.sortOrder === 'lowest'
      ? { price: 1 }
      : { price: -1 }
    : { _id: -1 };
  const courses = await Course.find({ ...category, ...searchKeyword }).sort(
    sortOrder
  );
  res.send(courses);
});

router.get('/:id', async (req, res) => {
  const course = await (await (await Course.findOne({ _id: req.params.id }).populate('videos')));
  if (course) {
    res.send(course);
  } else {
    res.status(404).send({ message: 'Course Not Found.' }); 
  }
});
router.post('/:id/reviews', isAuth, async (req, res) => {
  const course = await Course.findById(req.params.id);
  if (course) {
    const review = {
      name: req.body.name,
      rating: Number(req.body.rating),
      comment: req.body.comment,
    };
    course.reviews.push(review);
    course.numReviews = course.reviews.length;
    course.rating =
      course.reviews.reduce((a, c) => parseInt(c.rating) + a, 0) /
      course.reviews.length;
    const updatedCourse = await course.save();
    res.status(201).send({
      data: updatedCourse.reviews[updatedCourse.reviews.length - 1],
      message: 'Review saved successfully.',
    });
  } else {
    res.status(404).send({ message: 'Course Not Found' });
  }
});
router.put('/:id', isAuth, isAdmin, async (req, res) => {
  const courseId = req.params.id;
  const course = await Course.findById(courseId);
  if (course) {
    course.name = req.body.name;
    course.price = req.body.price;
    course.image = req.body.image;
    course.category = req.body.category;
    course.description = req.body.description;
    const updatedCourse = await course.save();
    if (updatedCourse) {
      return res
        .status(200)
        .send({ message: 'Course Updated', data: updatedCourse });
    }
  }
  return res.status(500).send({ message: ' Error in Updating Course.' });
});

router.delete('/:id', isAuth, isAdmin, async (req, res) => {
  const deletedCourse = await Course.findById(req.params.id);
  if (deletedCourse) {
    await deletedCourse.remove();
    res.send({ message: 'Course Deleted' });
  } else {
    res.send('Error in Deletion.');
  }
});

router.post('/', isAuth, isAdmin, async (req, res) => {
  const course = new Course({
    name: req.body.name,
    price: req.body.price,
    image: req.body.image,
    category: req.body.category,
    countInStock: req.body.countInStock,
    description: req.body.description,
    rating: req.body.rating,
    numReviews: req.body.numReviews,
  });
  const newCourse = await course.save();
  if (newCourse) {
    return res
      .status(201)
      .send({ message: 'New Course Created', data: newCourse });
  }
  return res.status(500).send({ message: ' Error in Creating Course.' });
});

router.put('/:id/videos', isAuth, isAdmin, async (req, res) => {
  const courseId = req.params.id;
  const course = await Course.findById(courseId);
  if (course) {
    const video = {
      id: req.body.videoId,
    };
    course.videos.push(video);
    const updatedCourse = await course.save();
    if (updatedCourse) {
      return res
        .status(200)
        .send({ message: 'Video added to course', data: updatedCourse });
    }
  }
  return res.status(500).send({ message: ' Error in Adding Video to Course.' });
});

router.put('/:id/videos/:videoid', isAuth, isAdmin, async (req, res) => {
    const courseId = req.params.id;
    const videoid = req.params.videoid;

    const course = await Course.findById(courseId);
    if (course) {
      const video = {
        id: videoid,
      };
      course.videos.pull(video);
      const updatedCourse = await course.save();
      if (updatedCourse) {
        return res
          .status(200)
          .send({ message: 'Video removed from course', data: updatedCourse });
      }
    }
    return res.status(500).send({ message: ' Error in removing Video from Course.' });
  });

export default router;

import express from 'express';
import Video from '../models/videoModel';
import { isAuth, isAdmin, isCourseSubscribed } from '../util';

const router = express.Router();

router.get('/', isAuth, isAdmin, async (req, res) => {
  const searchKeyword = req.query.searchKeyword
    ? {
        name: {
          $regex: req.query.searchKeyword,
          $options: 'i',
        },
      }
    : {};
  const sortOrder = req.query.sortOrder
    ? req.query.sortOrder === 'asc'
      ? { name: 1 }
      : { name: -1 }
    : { _id: -1 };
  const videos = await Video.find({ ...searchKeyword }).sort(
    sortOrder
  );
  res.send(videos);
});

router.get('/:id', isAuth, isCourseSubscribed, async (req, res) => {
  const video = await Video.findOne({ _id: req.params.id });
  if (video) {
    res.send(video);
  } else {
    res.status(404).send({ message: 'Video Not Found.' });
  }
});

router.put('/:id', isAuth, isAdmin, async (req, res) => {
  const videoId = req.params.id;
  const video = await Video.findById(videoId);
  if (video) {
    video.vimeoid = req.body.vimeoid;
    video.name = req.body.name;
    video.image = req.body.image;
    video.description = req.body.description;
    video.duration = req.body.duration;
    video.width = req.body.width;
    video.height = req.body.height;
    const updatedVideo = await video.save();
    if (updatedVideo) {
      return res
        .status(200)
        .send({ message: 'Video Updated', data: updatedVideo });
    }
  }
  return res.status(500).send({ message: ' Error in Updating Video.' });
});

router.delete('/:id', isAuth, isAdmin, async (req, res) => {
  const deletedVideo = await Video.findById(req.params.id);
  if (deletedVideo) {
    await deletedVideo.remove();
    res.send({ message: 'Video Deleted' });
  } else {
    res.send('Error in Deletion.');
  }
});

router.post('/', isAuth, isAdmin, async (req, res) => {
  console.log(req.body);
  const video = new Video({
    vimeoid: req.body.vimeoid,
    name: req.body.name,
    description: req.body.description,
    image: req.body.image,
    duration: req.body.duration,
    width: req.body.width,
    height: req.body.height,
  });
  const newVideo = await video.save();
  if (newVideo) {
    return res
      .status(201)
      .send({ message: 'New Video Created', data: newVideo });
  }
  return res.status(500).send({ message: ' Error in Creating Video.' });
});

export default router;

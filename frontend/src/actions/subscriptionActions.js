import Axios from "axios";
import {
  SUBSCRIPTION_CREATE_REQUEST, SUBSCRIPTION_CREATE_SUCCESS, SUBSCRIPTION_CREATE_FAIL,
  SUBSCRIPTION_DETAILS_REQUEST, SUBSCRIPTION_DETAILS_SUCCESS, SUBSCRIPTION_DETAILS_FAIL, SUBSCRIPTION_COURSE_REQUEST, SUBSCRIPTION_COURSE_SUCCESS, SUBSCRIPTION_COURSE_FAIL, SUBSCRIPTION_ACTIVE_REQUEST, SUBSCRIPTION_ACTIVE_SUCCESS, SUBSCRIPTION_ACTIVE_FAIL, MY_SUBSCRIPTION_LIST_REQUEST, MY_SUBSCRIPTION_LIST_SUCCESS, MY_SUBSCRIPTION_LIST_FAIL, SUBSCRIPTION_DELETE_REQUEST, SUBSCRIPTION_DELETE_SUCCESS, SUBSCRIPTION_DELETE_FAIL, SUBSCRIPTION_LIST_REQUEST, SUBSCRIPTION_LIST_SUCCESS, SUBSCRIPTION_LIST_FAIL
} from "../constants/subscriptionConstants";

const createSubscription = (subscription) => async (dispatch, getState) => {
  try {
    dispatch({ type: SUBSCRIPTION_CREATE_REQUEST, payload: subscription });
    const { userSignin: { userInfo } } = getState();
    const { data: { data: newSubscription } } = await Axios.post("/api/subscriptions", subscription, {
      headers: {
        Authorization: ' Bearer ' + userInfo.token
      }
    });
    dispatch({ type: SUBSCRIPTION_CREATE_SUCCESS, payload: newSubscription });
  } catch (error) {
    dispatch({ type: SUBSCRIPTION_CREATE_FAIL, payload: error.message });
  }
}

const listMySubscriptions = () => async (dispatch, getState) => {
  try {
    dispatch({ type: MY_SUBSCRIPTION_LIST_REQUEST });
    const { userSignin: { userInfo } } = getState();
    const { data } = await Axios.get("/api/subscriptions/mine", {
      headers:
        { Authorization: 'Bearer ' + userInfo.token }
    });
    dispatch({ type: MY_SUBSCRIPTION_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: MY_SUBSCRIPTION_LIST_FAIL, payload: error.message });
  }
}

const listSubscriptions = () => async (dispatch, getState) => {

  try {
    dispatch({ type: SUBSCRIPTION_LIST_REQUEST });
    const { userSignin: { userInfo } } = getState();
    const { data } = await Axios.get("/api/subscriptions", {
      headers:
        { Authorization: 'Bearer ' + userInfo.token }
    });
    dispatch({ type: SUBSCRIPTION_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: SUBSCRIPTION_LIST_FAIL, payload: error.message });
  }
}

const detailsSubscription = (subscriptionId) => async (dispatch, getState) => {
  try {
    dispatch({ type: SUBSCRIPTION_DETAILS_REQUEST, payload: subscriptionId });
    const { userSignin: { userInfo } } = getState();
    const { data } = await Axios.get("/api/subscriptions/" + subscriptionId, {
      headers:
        { Authorization: 'Bearer ' + userInfo.token }
    });
    dispatch({ type: SUBSCRIPTION_DETAILS_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: SUBSCRIPTION_DETAILS_FAIL, payload: error.message });
  }
}

const myCourseSubscription = (courseId) => async (dispatch, getState) => {
  try {
    dispatch({ type: SUBSCRIPTION_COURSE_REQUEST, payload: courseId });
    const { userSignin: { userInfo } } = getState();
    const { data } = await Axios.get("/api/subscriptions/course/" + courseId, {
      headers:
        { Authorization: 'Bearer ' + userInfo.token }
    });
    dispatch({ type: SUBSCRIPTION_COURSE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: SUBSCRIPTION_COURSE_FAIL, payload: error.message });
  }
}

const activeSubscription = (subscription, result) => async (dispatch, getState) => {
  try {
    dispatch({ type: SUBSCRIPTION_ACTIVE_REQUEST, payload: result });
    const { userSignin: { userInfo } } = getState();
    const { data } = await Axios.put("/api/subscriptions/" + subscription._id + "/active", result, {
      headers:
        { Authorization: 'Bearer ' + userInfo.token }
    });
    dispatch({ type: SUBSCRIPTION_ACTIVE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: SUBSCRIPTION_ACTIVE_FAIL, payload: error.message });
  }
}

const deleteSubscription = (subscriptionId) => async (dispatch, getState) => {
  try {
    dispatch({ type: SUBSCRIPTION_DELETE_REQUEST, payload: subscriptionId });
    const { userSignin: { userInfo } } = getState();
    const { data } = await Axios.delete("/api/subscriptions/" + subscriptionId, {
      headers:
        { Authorization: 'Bearer ' + userInfo.token }
    });
    dispatch({ type: SUBSCRIPTION_DELETE_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: SUBSCRIPTION_DELETE_FAIL, payload: error.message });
  }
}
export { 
  createSubscription, 
  detailsSubscription, 
  activeSubscription, 
  listMySubscriptions, 
  myCourseSubscription, 
  listSubscriptions, 
  deleteSubscription 
};
import Axios from "axios";
import Cookie from "js-cookie";
import { CART_ADD_ITEM, CART_REMOVE_ITEM, CART_SAVE_SHIPPING, CART_SAVE_PAYMENT } from "../constants/cartConstants";

const addToCart = (courseId) => async (dispatch, getState) => {
  try {
    const { data } = await Axios.get("/api/courses/" + courseId);
    dispatch({
      type: CART_ADD_ITEM, payload: {
        course: data._id,
        name: data.name,
        image: data.image,
        price: data.price
      }
    });
    const { cart: { cartItems } } = getState();
    Cookie.set("cartItems", JSON.stringify(cartItems));

  } catch (error) {

  }
}
const removeFromCart = (courseId) => (dispatch, getState) => {
  dispatch({ type: CART_REMOVE_ITEM, payload: courseId });

  const { cart: { cartItems } } = getState();
  Cookie.set("cartItems", JSON.stringify(cartItems));
}
const saveShipping = (data) => (dispatch) => {
  dispatch({ type: CART_SAVE_SHIPPING, payload: data });
}
const savePayment = (data) => (dispatch) => {
  dispatch({ type: CART_SAVE_PAYMENT, payload: data });
  Cookie.set("cartItems", JSON.stringify([]));
}
export { addToCart, removeFromCart, saveShipping, savePayment }
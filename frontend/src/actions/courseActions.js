import {
  COURSE_LIST_REQUEST,
  COURSE_LIST_SUCCESS,
  COURSE_LIST_FAIL,
  COURSE_DETAILS_REQUEST,
  COURSE_DETAILS_SUCCESS,
  COURSE_DETAILS_FAIL,
  COURSE_SAVE_REQUEST,
  COURSE_SAVE_SUCCESS,
  COURSE_SAVE_FAIL,
  COURSE_DELETE_SUCCESS,
  COURSE_DELETE_FAIL,
  COURSE_DELETE_REQUEST,
  COURSE_REVIEW_SAVE_REQUEST,
  COURSE_REVIEW_SAVE_FAIL,
  COURSE_REVIEW_SAVE_SUCCESS,
  COURSE_VIDEO_SAVE_REQUEST,
  COURSE_VIDEO_SAVE_FAIL,
  COURSE_VIDEO_SAVE_SUCCESS,
  COURSE_VIDEO_DELETE_REQUEST,
  COURSE_VIDEO_DELETE_FAIL,
  COURSE_VIDEO_DELETE_SUCCESS,
} from '../constants/courseConstants';
import axios from 'axios';
import Axios from 'axios';

const listCourses = (
  category = '',
  searchKeyword = '',
  sortOrder = ''
) => async (dispatch) => {
  try {
    dispatch({ type: COURSE_LIST_REQUEST });
    const { data } = await axios.get(
      '/api/courses?category=' +
        category +
        '&searchKeyword=' +
        searchKeyword +
        '&sortOrder=' +
        sortOrder
    );
    dispatch({ type: COURSE_LIST_SUCCESS, payload: data });
  } catch (error) {
    dispatch({ type: COURSE_LIST_FAIL, payload: error.message });
  }
};

const saveCourse = (course) => async (dispatch, getState) => {
  try {
    dispatch({ type: COURSE_SAVE_REQUEST, payload: course });
    const {
      userSignin: { userInfo },
    } = getState();
    if (!course._id) {
      const { data } = await Axios.post('/api/courses', course, {
        headers: {
          Authorization: 'Bearer ' + userInfo.token,
        },
      });
      dispatch({ type: COURSE_SAVE_SUCCESS, payload: data });
    } else {
      const { data } = await Axios.put(
        '/api/courses/' + course._id,
        course,
        {
          headers: {
            Authorization: 'Bearer ' + userInfo.token,
          },
        }
      );
      dispatch({ type: COURSE_SAVE_SUCCESS, payload: data });
    }
  } catch (error) {
    dispatch({ type: COURSE_SAVE_FAIL, payload: error.message });
  }
};

const detailsCourse = (courseId) => async (dispatch) => {
  try {
    dispatch({ type: COURSE_DETAILS_REQUEST, payload: courseId });
    const { data } = await axios.get('/api/courses/' + courseId);
    dispatch({ type: COURSE_DETAILS_SUCCESS, payload: data });
  } catch (error) {
    dispatch({ type: COURSE_DETAILS_FAIL, payload: error.message });
  }
};

const deleteCourse = (courseId) => async (dispatch, getState) => {
  try {
    const {
      userSignin: { userInfo },
    } = getState();
    dispatch({ type: COURSE_DELETE_REQUEST, payload: courseId });
    const { data } = await axios.delete('/api/courses/' + courseId, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token,
      },
    });
    dispatch({ type: COURSE_DELETE_SUCCESS, payload: data, success: true });
  } catch (error) {
    dispatch({ type: COURSE_DELETE_FAIL, payload: error.message });
  }
};

const saveCourseReview = (courseId, review) => async (dispatch, getState) => {
  try {
    const {
      userSignin: {
        userInfo: { token },
      },
    } = getState();
    dispatch({ type: COURSE_REVIEW_SAVE_REQUEST, payload: review });
    const { data } = await axios.post(
      `/api/courses/${courseId}/reviews`,
      review,
      {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      }
    );
    dispatch({ type: COURSE_REVIEW_SAVE_SUCCESS, payload: data });
  } catch (error) {
    // report error
    dispatch({ type: COURSE_REVIEW_SAVE_FAIL, payload: error.message });
  }
};

const addCourseVideo = (courseId, video) => async (dispatch, getState) => {
  try {
    const {
      userSignin: {
        userInfo: { token },
      },
    } = getState();
    dispatch({ type: COURSE_VIDEO_SAVE_REQUEST, payload: video });
    const { data } = await axios.post(
      `/api/courses/${courseId}/videos`,
      video,
      {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      }
    );
    dispatch({ type: COURSE_VIDEO_SAVE_SUCCESS, payload: data });
  } catch (error) {
    // report error
    dispatch({ type: COURSE_VIDEO_SAVE_FAIL, payload: error.message });
  }
};

const deleteCourseVideo = (courseId, video) => async (dispatch, getState) => {
  try {
    const {
      userSignin: {
        userInfo: { token },
      },
    } = getState();
    dispatch({ type: COURSE_VIDEO_DELETE_REQUEST, payload: video });
    const { data } = await axios.post(
      `/api/courses/${courseId}/videos/${video._id}`,
      video,
      {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      }
    );
    dispatch({ type: COURSE_VIDEO_DELETE_SUCCESS, payload: data });
  } catch (error) {
    // report error
    dispatch({ type: COURSE_VIDEO_DELETE_FAIL, payload: error.message });
  }
};

export {
  listCourses,
  detailsCourse,
  saveCourse,
  deleteCourse,
  saveCourseReview,
  addCourseVideo,
  deleteCourseVideo
};

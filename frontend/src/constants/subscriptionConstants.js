export const SUBSCRIPTION_CREATE_REQUEST = 'SUBSCRIPTION_CREATE_REQUEST';
export const SUBSCRIPTION_CREATE_SUCCESS = 'SUBSCRIPTION_CREATE_SUCCESS';
export const SUBSCRIPTION_CREATE_FAIL = 'SUBSCRIPTION_CREATE_FAIL';

export const SUBSCRIPTION_DETAILS_REQUEST = 'SUBSCRIPTION_DETAILS_REQUEST';
export const SUBSCRIPTION_DETAILS_SUCCESS = 'SUBSCRIPTION_DETAILS_SUCCESS';
export const SUBSCRIPTION_DETAILS_FAIL = 'SUBSCRIPTION_DETAILS_FAIL';

export const SUBSCRIPTION_COURSE_REQUEST = 'SUBSCRIPTION_COURSE_REQUEST';
export const SUBSCRIPTION_COURSE_SUCCESS = 'SUBSCRIPTION_COURSE_SUCCESS';
export const SUBSCRIPTION_COURSE_FAIL = 'SUBSCRIPTION_COURSE_FAIL';

export const MY_SUBSCRIPTION_LIST_REQUEST = 'MY_SUBSCRIPTION_LIST_REQUEST';
export const MY_SUBSCRIPTION_LIST_SUCCESS = 'MY_SUBSCRIPTION_LIST_SUCCESS';
export const MY_SUBSCRIPTION_LIST_FAIL = 'MY_SUBSCRIPTION_LIST_FAIL';

export const SUBSCRIPTION_LIST_REQUEST = 'SUBSCRIPTION_LIST_REQUEST';
export const SUBSCRIPTION_LIST_SUCCESS = 'SUBSCRIPTION_LIST_SUCCESS';
export const SUBSCRIPTION_LIST_FAIL = 'SUBSCRIPTION_LIST_FAIL';

export const SUBSCRIPTION_ACTIVE_REQUEST = 'SUBSCRIPTION_ACTIVE_REQUEST';
export const SUBSCRIPTION_ACTIVE_SUCCESS = 'SUBSCRIPTION_ACTIVE_SUCCESS';
export const SUBSCRIPTION_ACTIVE_FAIL = 'SUBSCRIPTION_ACTIVE_FAIL';

export const SUBSCRIPTION_DELETE_REQUEST = 'SUBSCRIPTION_DELETE_REQUEST';
export const SUBSCRIPTION_DELETE_SUCCESS = 'SUBSCRIPTION_DELETE_SUCCESS';
export const SUBSCRIPTION_DELETE_FAIL = 'SUBSCRIPTION_DELETE_FAIL';
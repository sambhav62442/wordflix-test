import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { signin } from '../actions/userActions';

function SigninScreen(props) {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const userSignin = useSelector(state => state.userSignin);
  const { loading, userInfo, error } = userSignin;
  const dispatch = useDispatch();
  const redirect = props.location.search ? props.location.search.split("=")[1] : '/';
  useEffect(() => {
    if (userInfo) {
      props.history.push(redirect);
    }
    return () => {
      //
    };
  }, [userInfo]);

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(signin(email, password));

  }
  return <div className="form text-center">
    <form className="form-signin" onSubmit={submitHandler}>
      <img className="mb-4" src="/logo512.png" alt="" width="72" height="72"></img>
      <h1 className="h3 mb-3 font-weight-bold text-left">Sign In</h1>
      {loading && <div>Loading...</div>}
      {error && <div className="error">{error}</div>}
      <label htmlFor="email" className="sr-only">Email address</label>
      <input type="email" id="email" onChange={(e) => setEmail(e.target.value)} className="form-control" placeholder="Email address" required="" autofocus=""></input>
      <label htmlFor="password" className="sr-only">Password</label>
      <input type="password" id="password" onChange={(e) => setPassword(e.target.value)} className="form-control" placeholder="Password" required=""></input>
      <button className="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <div className="mt-2">New to Wordflix? <Link to={redirect === "/" ? "register" : "register?redirect=" + redirect} className="text-center text-danger"><strong>Sign up now</strong></Link>.</div>
    </form>
    </div>
}
export default SigninScreen;
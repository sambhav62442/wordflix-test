import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios';
import {
  saveVideo,
  listVideos,
  deleteVideo,
} from '../actions/videoActions';

function VideosScreen(props) {
  const [modalVisible, setModalVisible] = useState(false);
  const [id, setId] = useState('');
  const [vimeoid, setVimeoid] = useState('');
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [image, setImage] = useState('');
  const [duration, setDuration] = useState('');
  const [width, setWidth] = useState('');
  const [height, setHeight] = useState('');
  const [uploading, setUploading] = useState(false);
  const videoList = useSelector((state) => state.videoList);
  const { loading, videos, error } = videoList;

  const videoSave = useSelector((state) => state.videoSave);
  const {
    loading: loadingSave,
    success: successSave,
    error: errorSave,
  } = videoSave;

  const videoDelete = useSelector((state) => state.videoDelete);
  const {
    loading: loadingDelete,
    success: successDelete,
    error: errorDelete,
  } = videoDelete;
  const dispatch = useDispatch();

  useEffect(() => {
    if (successSave) {
      setModalVisible(false);
    }
    dispatch(listVideos());
    return () => {
      //
    };
  }, [successSave, successDelete]);

  const openModal = (video) => {
    setModalVisible(true);
    setId(video._id);
    setVimeoid(video.vimeoid);
    setName(video.name);
    setDescription(video.description);
    setImage(video.image);
    setDuration(video.duration);
    setWidth(video.width);
    setHeight(video.height);
  };
  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(
      saveVideo({
        _id: id,
        vimeoid,
        name,
        description,
        image,
        duration,
        width,
        height
      })
    );
  };
  const deleteHandler = (video) => {
    dispatch(deleteVideo(video._id));
  };
  const uploadFileHandler = (e) => {
    const file = e.target.files[0];
    const bodyFormData = new FormData();
    bodyFormData.append('image', file);
    setUploading(true);
    axios
      .post('/api/uploads', bodyFormData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })
      .then((response) => {
        setImage(response.data);
        setUploading(false);
      })
      .catch((err) => {
        console.log(err);
        setUploading(false);
      });
  };
  return (
    <div className="content content-margined">
      <div className="video-header">
        <h3>Videos</h3>
        <button className="button primary" onClick={() => openModal({})}>
          Create Video
        </button>
      </div>
      {modalVisible && (
        <div className="form">
          <form onSubmit={submitHandler}>
            <ul className="form-container">
              <li>
                <h2>Create Video</h2>
              </li>
              <li>
                {loadingSave && <div>Loading...</div>}
                {errorSave && <div>{errorSave}</div>}
              </li>

              <li>
                <label htmlFor="vimeoid">Vimeo ID</label>
                <input
                  type="number"
                  name="vimeoid"
                  value={vimeoid}
                  id="vimeoid"
                  onChange={(e) => setVimeoid(e.target.value)}
                ></input>
              </li>
              <li>
                <label htmlFor="name">Name</label>
                <input
                  type="text"
                  name="name"
                  value={name}
                  id="name"
                  onChange={(e) => setName(e.target.value)}
                ></input>
              </li>
              <li>
                <label htmlFor="description">Description</label>
                <textarea
                  name="description"
                  value={description}
                  id="description"
                  onChange={(e) => setDescription(e.target.value)}
                ></textarea>
              </li>
              <li>
                <label htmlFor="image">Image</label>
                <input
                  type="text"
                  name="image"
                  value={image}
                  id="image"
                  onChange={(e) => setImage(e.target.value)}
                ></input>
                <input type="file" onChange={uploadFileHandler}></input>
                {uploading && <div>Uploading...</div>}
              </li>
              <li>
                <label htmlFor="duration">Duration</label>
                <input
                  type="text"
                  name="duration"
                  value={duration}
                  id="duration"
                  onChange={(e) => setDuration(e.target.value)}
                ></input>
              </li>
              <li>
                <label htmlFor="width">Width</label>
                <input
                  type="text"
                  name="width"
                  value={width}
                  id="width"
                  onChange={(e) => setWidth(e.target.value)}
                ></input>
              </li>
              <li>
                <label htmlFor="height">Height</label>
                <input
                  type="text"
                  name="height"
                  value={height}
                  id="height"
                  onChange={(e) => setHeight(e.target.value)}
                ></input>
              </li>
              <li>
                <button type="submit" className="button primary">
                  {id ? 'Update' : 'Create'}
                </button>
              </li>
              <li>
                <button
                  type="button"
                  onClick={() => setModalVisible(false)}
                  className="button secondary"
                >
                  Back
                </button>
              </li>
            </ul>
          </form>
        </div>
      )}

      <div className="video-list">
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>VimeoID</th>
              <th>Name</th>
              <th>Duration</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {videos.map((video) => (
              <tr key={video._id}>
                <td>{video._id}</td>
                <td>{video.vimeoid}</td>
                <td>{video.name}</td>
                <td>{video.duration}</td>
                <td>
                  <button className="button" onClick={() => openModal(video)}>
                    Edit
                  </button>{' '}
                  <button
                    className="button"
                    onClick={() => deleteHandler(video)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
export default VideosScreen;

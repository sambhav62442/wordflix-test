import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { detailsCourse, saveCourseReview } from '../actions/courseActions';
import { listMyOrders } from '../actions/orderActions';
import { myCourseSubscription } from '../actions/subscriptionActions';
import Rating from '../components/Rating';
import { COURSE_REVIEW_SAVE_RESET } from '../constants/courseConstants';
import  ProfileIcon from '../components/ProfileIcon';

function CourseScreen(props) {
  const [rating, setRating] = useState(0);
  const [comment, setComment] = useState('');
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;
  const courseDetails = useSelector((state) => state.courseDetails);
  const { course, loading, error } = courseDetails;
  const myOrderList = useSelector(state => state.myOrderList);
  const { loading: loadingOrders, orders, error: errorOrders } = myOrderList;
  const courseSubscription = useSelector(state => state.courseSubscription);
  const { subscription } = courseSubscription;
  const courseReviewSave = useSelector((state) => state.courseReviewSave);
  const { success: courseSaveSuccess } = courseReviewSave;
  const dispatch = useDispatch();

  // console.log(subscription);
  useEffect(() => {
    if (courseSaveSuccess) {
      alert('Review submitted successfully.');
      setRating(0);
      setComment('');
      dispatch({ type: COURSE_REVIEW_SAVE_RESET });
    }
    dispatch(detailsCourse(props.match.params.id));

    dispatch(myCourseSubscription(props.match.params.id));

    dispatch(listMyOrders());
    return () => {
      //
    };
  }, [courseSaveSuccess]);

  console.log(subscription);
  const submitHandler = (e) => {
    e.preventDefault();
    // dispatch actions
    dispatch(
      saveCourseReview(props.match.params.id, {
        name: userInfo.name,
        rating: rating,
        comment: comment,
      })
    );
  };
  const handleAddToCart = () => {
    props.history.push('/cart/' + props.match.params.id);
  };

  const handleCourseVideos = () => {
    props.history.push(`/course/${props.match.params.id}/${course.videos[0]._id}`);
  }

  return (
    <div>
      <div className="back-to-result">
        <Link to="/">Back to result</Link>
      </div>
      {loading ? (
        <div>Loading...</div>
      ) : error ? (
        <div>{error} </div>
      ) : (
        <>
          <header className="courseHeader">
            <div className="details row">
              <div className="details-image col-md-6 col-sm-12 mb-4">
                <img src={course.image} alt={`${course.name}'s picture`}/>
              </div>
              <div className="details-info col-md-6 col-sm-12">
                <h2>{course.name}</h2>
                <a href="#reviews">
                  <Rating
                    value={course.rating}
                    text={course.numReviews + ' reviews'}
                  />
                </a>
                <div className="details-action">
                  {(subscription && subscription.status == 1) ? 
                  (<button onClick={handleCourseVideos} className="button btn-red nmhp-cta nmhp-cta-large btn-none btn-custom">GO TO THE COURSE</button>)
                  :(
                  <div>
                    <div className="details-price"><strong>₹{course.price}</strong></div>
                    <button onClick={handleAddToCart} className="button btn-red nmhp-cta nmhp-cta-large btn-none btn-custom">ADD TO CART</button>
                  </div>)}
                </div>
              </div>
            </div>
          </header>
          <section className="courseDescription">
            <h2>Description</h2>
            <div>{course.description}</div>
          </section>
          <section>
            <h2>Videos</h2>
            {!course.videos.length ? (<div>There is no video</div>) : 
            <ul className="videos" id="videos">
              <li>
                <div>{course.videos.length} Videos</div>
              </li>
            </ul>}
          </section>
          <section>
            <h2>Reviews</h2>
            {!course.reviews.length && <div>There is no review</div>}
            <ul className="review" id="reviews">
            {(subscription && subscription.status == 1) ?
            (<li style={{marginBottom:"2em"}}>
                <h4>Your Review</h4>
                {userInfo ? (
                  <form onSubmit={submitHandler}>
                    <ul className="form-container">
                      <li>
                        <label className="label" htmlFor="rating">Rating</label>
                        <select
                          className="form-control"
                          name="rating"
                          id="rating"
                          value={rating}
                          onChange={(e) => setRating(e.target.value)}
                        >
                          <option value="1">1- Poor</option>
                          <option value="2">2- Fair</option>
                          <option value="3">3- Good</option>
                          <option value="4">4- Very Good</option>
                          <option value="5">5- Excelent</option>
                        </select>
                      </li>
                      <li>
                        <label htmlFor="comment">Comment</label>
                        <textarea
                          className="form-control"
                          name="comment"
                          value={comment}
                          onChange={(e) => setComment(e.target.value)}
                        ></textarea>
                      </li>
                      <li>
                        <button type="submit" className="btn btn-lg btn-primary btn-block">
                          SUBMIT
                        </button>
                      </li>
                    </ul>
                  </form>
                ) : (
                  <div>
                    Please <Link to="/signin">Sign-in</Link> to write a review.
                  </div>
                )}
              </li> ) : (' ')}
              {course.reviews.map((review) => (
              <li key={review._id} style={{display:"flex", flexDirection:"row", marginBottom:"10px"}}>
                <div className="reviewIcon" style={{display:"block", padding:"0 1rem 0 0"}}><ProfileIcon fontSize="1.2em" value="SJ" color="rgb(115, 114, 108)"/></div>
                <div className="reviewDetails">
                  <div className="font-weight-bold">{review.name}</div>
                  <div>
                    <Rating value={review.rating}></Rating>
                  </div>
                  <div style={{fontSize:"0.8em", color:"rgba(0, 0, 0, 0.5)"}}>{new Intl.DateTimeFormat('en-US', {year: 'numeric', month: 'short',day: '2-digit'}).format(new Date(review.createdAt))}</div>
                  <div>{review.comment}</div>
                </div>
              </li>
              ))}
            </ul>
          </section>
        </>
      )}
    </div>
  );
}
export default CourseScreen;

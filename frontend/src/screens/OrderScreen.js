import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { createOrder, detailsOrder, payOrder } from '../actions/orderActions';
import { createSubscription } from '../actions/subscriptionActions';
import { savePayment } from '../actions/cartActions';
import PaypalButton from '../components/PaypalButton';
function OrderScreen(props) {

  const orderPay = useSelector(state => state.orderPay);
  const { loading: loadingPay, success: successPay, error: errorPay } = orderPay;
  const [paymentMethod, setPaymentMethod] = useState('');
  const dispatch = useDispatch();
  useEffect(() => {
    if (successPay) {
      props.history.push("/profile");
    } else {
      dispatch(detailsOrder(props.match.params.id));
    }
    return () => {
    };
  }, [successPay]);

  const orderDetails = useSelector(state => state.orderDetails);
  const { loading, order, error } = orderDetails;

  const handleSuccessPayment = (paymentResult) => {
    paymentResult.payerID = paymentResult.payerID ? paymentResult.payerID : '123456';
    paymentResult.orderID = paymentResult.payerID ? paymentResult.payerID : '123456';
    paymentResult.paymentID = paymentResult.payerID ? paymentResult.payerID : '123456';
    paymentResult.paymentMethod = paymentMethod;
    dispatch(createSubscription(order));
    order.orderItems.map(subscriptionCourse => {
      subscriptionCourse.user = order.user;
      subscriptionCourse.status = 1;
      subscriptionCourse.expiryDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
      dispatch(createSubscription(subscriptionCourse));
    });
    dispatch(payOrder(order, paymentResult));
  }

  console.log(order);

  return loading ? <div>Loading ...</div> : error ? <div>{error}</div> :

    <div>
      <div className="placeorder">
        <div className="placeorder-info">
          <div>
            <ul className="cart-list-container m-0 p-0">
              <li>
                <h3>
                  Shopping Cart
          </h3>
                <div>
                  Price
          </div>
              </li>
              {
                order.orderItems.length === 0 ?
                  <div>
                    Cart is empty
          </div>
                  :
                  order.orderItems.map(item =>
                    <li key={item._id}>
                      <div className="cart-image">
                        <img src={item.image} alt="course" />
                      </div>
                      <div className="cart-name">
                        <div>
                          <Link to={"/course/" + item.course}>
                            {item.name}
                          </Link>
                        </div>
                      </div>
                      <div className="cart-price">
                        ₹{item.price}
                      </div>
                    </li>
                  )
              }
            </ul>
          </div>
        </div>
        <div className="placeorder-action">
          <ul>
            <li>
              <div>
                <h3>Payment</h3>
                {(!order.isPaid) ? (
                <div>
                  <div className="form-check mb-2 ml-2">
                  <input className="form-check-input" type="radio" name="paymentMethod" id="paypalPaymentMethod" value="paypal" onChange={(e) => setPaymentMethod(e.target.value)}></input>
                  <label className="form-check-label" htmlFor="paypalPaymentMethod">PayPal</label>
                </div>
                <div className="form-check mb-2 ml-2">
                  <input className="form-check-input" type="radio" name="paymentMethod" id="DemoPaymentMethod" value="demopayment" onChange={(e) => setPaymentMethod(e.target.value)}></input>
                  <label className="form-check-label" htmlFor="DemoPaymentMethod">Demo Payment</label>
                </div>
                </div>
                ) : (`Paid on ${new Intl.DateTimeFormat('en-US', {year: 'numeric', month: 'short',day: '2-digit'}).format(new Date(order.paidAt))} via ${order.payment.paymentMethod}`)
                }
              </div>
            </li>
            {!order.isPaid &&
            <li className="placeorder-actions-payment">
              {loadingPay && <div>Finishing Payment...</div>}
              {(paymentMethod == 'paypal') ? (
                <PaypalButton
                  amount={order.totalPrice}
                  onSuccess={handleSuccessPayment} />
                ) : (paymentMethod == 'demopayment') ? (
                  <button className="btn btn-lg btn-primary btn-block" onClick={handleSuccessPayment}>Pay</button>
                ) : (' ')
              }
            </li>
            }
            <li>
              <h3>Order Summary</h3>
            </li>
            <li>
              <div>Items</div>
              <div>₹{order.itemPrice}</div>
            </li>
            <li>
              <div>Tax</div>
              <div>₹{order.taxPrice}</div>
            </li>
            <li>
              <div>Order Total</div>
              <div>₹{order.totalPrice}</div>
            </li>
          </ul>
        </div>
      </div>
    </div>
}

export default OrderScreen;
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import Player from '@vimeo/player';
import { detailsCourse, saveCourseReview } from '../actions/courseActions';
import {detailsVideo} from '../actions/videoActions';
import { COURSE_REVIEW_SAVE_RESET } from '../constants/courseConstants';
import VimeoPlayer from '../components/VimeoPlayer';
import VideoSearch from '../components/VideoSearch';

function CourseVideoScreen(props) {
  const [rating, setRating] = useState(0);
  const [comment, setComment] = useState('');
  const [videoId, setVideoId] = useState(props.match.params.videoId);
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;
  const courseDetails = useSelector((state) => state.courseDetails);
  const { course, loading, error } = courseDetails;
  const videoDetails = useSelector((state) => state.videoDetails);
  const { video, loading: vloading, error: verror } = videoDetails;
  const courseReviewSave = useSelector((state) => state.courseReviewSave);
  const { success: courseSaveSuccess } = courseReviewSave;
  const [nextVideo, setNextVideo] = useState('');
  const dispatch = useDispatch();

  const vimeoScript = async () => {
    var iframe = document.querySelector("#vimeo_player");
    var player = new Player(iframe);
    player.play();
    player.on("ended", () => {
      let liTag = document.getElementById(videoId);
      if(!liTag.dataset.next == '') {
        props.history.push(liTag.dataset.next);
        setVideoId(liTag.dataset.next);
      }
      console.log('ended');
    })
  }

  useEffect(() => {
    if (courseSaveSuccess) {
      alert('Review submitted successfully.');
      setRating(0);
      setComment('');
      dispatch({ type: COURSE_REVIEW_SAVE_RESET });
    }
    if(videoId != null) {
      dispatch(detailsVideo(videoId));
    }
    setTimeout(function(){ vimeoScript(); }, 1000);

    dispatch(detailsCourse(props.match.params.id));
    return () => {
      //
    };
  }, [courseSaveSuccess, videoId]);

  const submitHandler = (e) => {
    e.preventDefault();
    // dispatch actions
    dispatch(
      saveCourseReview(props.match.params.id, {
        name: userInfo.name,
        rating: rating,
        comment: comment,
      })
    );
  };

  const changeVideo = (videoId) => {
    props.history.push(videoId);
    setVideoId(videoId);
  }

  return (
    <div>
      <div className="back-to-result">
        <Link to={`/course/${props.match.params.id}`}>Back to result</Link>
      </div>
      {loading ? (
        <div>Loading...</div>
      ) : error ? (
        <div class="error">{error} </div>
      ) : vloading ? (
        <div>Loading Video...</div>
      ) : verror ? (
        <div>{verror} </div>
      ) : (
        <>
          <div className="details row">
            <div className="col-md-9 col-sm-12">
              <div className="details-image">
                <VimeoPlayer vimeoid={video.vimeoid}></VimeoPlayer>
              </div>
              <div className="details-info">
                <h3>{video.name}</h3>
                <div className="description">{video.description}</div>
              </div>
            </div>
            <div className="sidebar-details col-md-3 col-sm-12">
              <VideoSearch courseVideos={course.videos} onVideoChange={changeVideo}/>
            </div>
          </div>
        </>
      )}
    </div>
  );
}
export default CourseVideoScreen;

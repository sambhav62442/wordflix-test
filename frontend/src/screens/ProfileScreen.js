import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { logout, update } from '../actions/userActions';
import { listMyOrders } from '../actions/orderActions';
import { useDispatch, useSelector } from 'react-redux';
import { listMySubscriptions } from '../actions/subscriptionActions';

function ProfileScreen(props) {
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [initials, setInitials] = useState('');
  const dispatch = useDispatch();

  const userSignin = useSelector(state => state.userSignin);
  const { userInfo } = userSignin;
  const handleLogout = () => {
    dispatch(logout());
    props.history.push("/signin");
  }
  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(update({ userId: userInfo._id, email, name, password }))
  }
  const userUpdate = useSelector(state => state.userUpdate);
  const { loading, success, error } = userUpdate;

  const myOrderList = useSelector(state => state.myOrderList);
  const { loading: loadingOrders, orders, error: errorOrders } = myOrderList;

  const mySubscriptionList = useSelector(state => state.mySubscriptionList);
  const { loading: loadingSubscriptions, subscriptions, error: errorSubscriptions } = mySubscriptionList;
  useEffect(() => {
    if (userInfo) {
      console.log(userInfo.name)
      setEmail(userInfo.email);
      setName(userInfo.name);
      setPassword(userInfo.password);
      // var initials = name.match(/\b\w/g) || [];
      setInitials(userInfo.name.split(" ").map((n)=>n[0]).join(""));
    }
    dispatch(listMyOrders());
    dispatch(listMySubscriptions());
    return () => {

    };
  }, [userInfo])
  console.log(initials);
  return <div className="profile">
      <div className="profile-info">
        <div className="form">
          <form className="form-profile" onSubmit={submitHandler}>
            <h1 className="h3 mb-3 font-weight-bold text-left">User Profile</h1>
            {loading && <div>Loading...</div>}
            {error && <div className="error">{error}</div>}
            {success && <div>Profile Saved Successfully.</div>}
            <label htmlFor="name" className="sr-only">Name</label>      
            <input type="name" id="name" value={name} onChange={(e) => setName(e.target.value)} className="form-control" placeholder="Name" required=""></input>
            <label htmlFor="email" className="sr-only">Email address</label>
            <input type="email" id="email" value={email} onChange={(e) => setEmail(e.target.value)} className="form-control" placeholder="Email address" required=""></input>
            <label htmlFor="password" className="sr-only">Password</label>
            <input type="password" id="password" value={password} onChange={(e) => setPassword(e.target.value)} className="form-control" placeholder="Password" required=""></input>
            <button className="btn btn-lg btn-primary btn-block" type="submit">Update</button>
            <button className="btn btn-lg btn-secondary btn-block" onClick={handleLogout} type="button">Logout</button>
          </form>
        </div>
      </div>
      <div className="profile-orders content-margined">
      <h3>Your Subscriptions</h3>
      {
        loadingSubscriptions ? <div>Loading...</div> :
          errorSubscriptions ? <div>{errorSubscriptions} </div> :
            <table className="table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Date</th>
                  <th>Course</th>
                  <th>Status</th>
                  <th>Expiry Date</th>
                </tr>
              </thead>
              <tbody>
                {subscriptions.map(subscription => <tr key={subscription._id}>
                  <td>{subscription._id}</td>
                  <td>{new Intl.DateTimeFormat('en-US', {year: 'numeric', month: 'short',day: '2-digit'}).format(new Date(subscription.createdAt))}</td>
                  <td>{subscription.course.name}</td>
                  <td>{(subscription.status == 1) ? 'Active' : 'Not Active'}</td>
                  <td>{new Intl.DateTimeFormat('en-US', {year: 'numeric', month: 'short',day: '2-digit'}).format(new Date(subscription.expiryDate))}</td>
                </tr>)}
              </tbody>
            </table>
      }
    </div>
    <div className="profile-orders content-margined">
      <h3>Past Orders</h3>
      {
        loadingOrders ? <div>Loading...</div> :
          errorOrders ? <div>{errorOrders} </div> :
            <table className="table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>DATE</th>
                  <th>TOTAL</th>
                  <th>PAID</th>
                  <th>ACTIONS</th>
                </tr>
              </thead>
              <tbody>
                {orders.map(order => <tr key={order._id}>
                  <td>{order._id}</td>
                  <td>{new Intl.DateTimeFormat('en-US', {year: 'numeric', month: 'short',day: '2-digit'}).format(new Date(order.createdAt))}</td>
                  <td>{order.totalPrice}</td>
                  <td>{order.isPaid ? 'Paid' : 'Not Paid'}</td>
                  <td>
                    <Link to={"/order/" + order._id}>DETAILS</Link>
                  </td>
                </tr>)}
              </tbody>
            </table>
      }
    </div>
  </div>
}

export default ProfileScreen;
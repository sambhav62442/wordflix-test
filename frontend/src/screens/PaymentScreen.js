import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { savePayment } from '../actions/cartActions';
import CheckoutSteps from '../components/CheckoutSteps';

function PaymentScreen(props) {
  const [paymentMethod, setPaymentMethod] = useState('');

  const dispatch = useDispatch();

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(savePayment({ paymentMethod }));
    props.history.push('placeorder');
  };
  return (
    <div>
      <CheckoutSteps step1 step2></CheckoutSteps>
      <div className="form">
        <form onSubmit={submitHandler}>
          <div className="border p-4 rounded">
            <h2 className="mb-3">Payment</h2>
            <div className="form-check mb-2">
              <input className="form-check-input" type="radio" name="paymentMethod" id="paymentMethod" value="paypal" onChange={(e) => setPaymentMethod(e.target.value)}></input>
              <label className="form-check-label" htmlFor="paymentMethod">PayPal</label>
            </div>
            <button type="submit" className="btn btn-lg btn-primary btn-block">Continue</button>
          </div>
        </form>
      </div>
    </div>
  );
}
export default PaymentScreen;

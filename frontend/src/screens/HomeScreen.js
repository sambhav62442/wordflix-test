import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { listCourses } from '../actions/courseActions';
import Rating from '../components/Rating';

function HomeScreen(props) {
  const [searchKeyword, setSearchKeyword] = useState('');
  const [sortOrder, setSortOrder] = useState('');
  const category = props.match.params.id ? props.match.params.id : '';
  const courseList = useSelector((state) => state.courseList);
  const { courses, loading, error } = courseList;
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(listCourses(category));

    return () => {
      //
    };
  }, [category]);

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(listCourses(category, searchKeyword, sortOrder));
  };
  const sortHandler = (e) => {
    setSortOrder(e.target.value);
    dispatch(listCourses(category, searchKeyword, sortOrder));
  };

  return (
    <>
      {category && <h2>{category}</h2>}

      {/* <ul className="filter">
        <li>
          <form onSubmit={submitHandler}>
            <input
              name="searchKeyword"
              onChange={(e) => setSearchKeyword(e.target.value)}
            />
            <button type="submit">Search</button>
          </form>
        </li>
        <li>
          Sort By{' '}
          <select name="sortOrder" onChange={sortHandler}>
            <option value="asc">Ascending</option>
            <option value="des">Descending</option>
          </select>
        </li>
      </ul> */}
      {/* <div id="carouselExampleSlidesOnly" className="carousel slide" data-ride="carousel">
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img className="d-block w-100" src="/images/banner.jpg" alt="First slide"></img>
          </div>
          <div className="carousel-item">
            <img className="d-block w-100" src="" alt="Second slide"></img>
          </div>
          <div className="carousel-item">
            <img className="d-block w-100" src="" alt="Third slide"></img>
          </div>
        </div>
      </div> */}
      {loading ? (
        <div>Loading...</div>
      ) : error ? (
        <div>{error}</div>
      ) : (
        <div className="courses row">
          {courses.map((course) => (
              <div className="course col-md-3 col-sm-12 text-center">
                <Link to={'/course/' + course._id}>
                  <img
                    className="course-image"
                    src={course.image}
                    alt="course"
                  />
                  <div className="course-name text-left">
                    {course.name}
                  </div>
                  <div className="course-rating text-left">
                    <Rating
                      value={course.rating}
                      text={'('+course.numReviews+')'}
                    />
                  </div>
                  <div className="course-price text-left">₹{course.price}</div>
                </Link>
              </div>
          ))}
        </div>
      )}
    </>
  );
}
export default HomeScreen;

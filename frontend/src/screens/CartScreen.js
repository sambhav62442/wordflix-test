import React, { useEffect, useState } from 'react';
import { addToCart, removeFromCart } from '../actions/cartActions';
import { useDispatch, useSelector } from 'react-redux';
import { createOrder } from '../actions/orderActions';
function CartScreen(props) {

  const cart = useSelector(state => state.cart);
  const orderCreate = useSelector(state => state.orderCreate);
  const userSignin = useSelector(state => state.userSignin);
  const { userInfo } = userSignin;
  const { loading, success, error, order } = orderCreate;

  const { cartItems, payment } = cart;

  const itemPrice = cartItems.reduce((a, c) => a + parseInt(c.price), 0);
  const taxPrice = 0 * itemPrice;
  const totalPrice = itemPrice + taxPrice;

  const courseId = props.match.params.id;
  const dispatch = useDispatch();
  const removeFromCartHandler = (courseId) => {
    dispatch(removeFromCart(courseId));
  }

  const checkoutHandler = () => {
    if(userInfo) {
      dispatch(createOrder({
        orderItems: cartItems, payment, itemPrice,
        taxPrice, totalPrice
      }));
    } else {
      props.history.push("/signin?redirect=cart");
    }
  }  

  useEffect(() => {
    if (courseId) {
      dispatch(addToCart(courseId));
    }
    if (success) {
      props.history.push("/order/" + order._id);
    }
  }, [success]);

  return <div className="cart">
    <div className="cart-list">
      <h3>Shopping Cart</h3>
      <ul className="cart-list-container px-2 py-4">
        {
          cartItems.length === 0 ?
            <div>
              Cart is empty
          </div>
            :
            cartItems.map(item =>
              <li key={item.name}>
                <div className="cart-image">
                <a href={"/course/" + item.course}>
                  <img src={item.image} alt="course" />
                </a>
                </div>
                <div className="cart-name">
                  <div className="font-weight-bold text-dark">
                    <a className="text-dark" style={{textDecoration:"none"}} href={"/course/" + item.course}>
                      {item.name}
                    </a>
                  </div>
                  <div>
                    <a className="text-secondary small" style={{cursor:"pointer"}} onClick={() => removeFromCartHandler(item.course)} >Remove</a>
                  </div>
                </div>
                <div className="cart-price">
                  ₹{item.price}
                </div>
              </li>
            )
        }
      </ul>

    </div>
    <div className="cart-action">
      <div className="text-secondary">Total:</div>
      <h3 className="mt-3 mb-3">
         ₹{cartItems.reduce((a, c) => a + parseInt(c.price), 0)}
      </h3>
      <button onClick={checkoutHandler} className="btn btn-lg btn-primary btn-block" disabled={cartItems.length === 0}>
        Proceed to Checkout
      </button>

    </div>

  </div>
}

export default CartScreen;
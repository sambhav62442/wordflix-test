import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import Cookie from 'js-cookie';
import {
  courseListReducer,
  courseDetailsReducer,
  courseSaveReducer,
  courseDeleteReducer,
  courseReviewSaveReducer,
} from './reducers/courseReducers';
import {
  videoListReducer,
  videoDetailsReducer,
  videoSaveReducer,
  videoDeleteReducer
} from './reducers/videoReducers';
import { cartReducer } from './reducers/cartReducers';
import {
  userSigninReducer,
  userRegisterReducer,
  userUpdateReducer,
} from './reducers/userReducers';
import {
  orderCreateReducer,
  orderDetailsReducer,
  orderPayReducer,
  myOrderListReducer,
  orderListReducer,
  orderDeleteReducer,
} from './reducers/orderReducers';
import {
  subscriptionActiveReducer,
  subscriptionCreateReducer,
  subscriptionDeleteReducer,
  subscriptionDetailsReducer,
  subscriptionListReducer,
  mySubscriptionListReducer,
  courseSubscriptionReducer
} from './reducers/subscriptionReducers';

const cartItems = Cookie.getJSON('cartItems') || [];
const userInfo = Cookie.getJSON('userInfo') || null;

const initialState = {
  cart: { cartItems, payment: {paymentMethod: 'none'} },
  userSignin: { userInfo },
};
const reducer = combineReducers({
  courseList: courseListReducer,
  courseDetails: courseDetailsReducer,
  videoList: videoListReducer,
  videoDetails: videoDetailsReducer,
  cart: cartReducer,
  userSignin: userSigninReducer,
  userRegister: userRegisterReducer,
  courseSave: courseSaveReducer,
  courseDelete: courseDeleteReducer,
  courseReviewSave: courseReviewSaveReducer,
  videoSave: videoSaveReducer,
  videoDelete: videoDeleteReducer,
  orderCreate: orderCreateReducer,
  orderDetails: orderDetailsReducer,
  orderPay: orderPayReducer,
  userUpdate: userUpdateReducer,
  myOrderList: myOrderListReducer,
  orderList: orderListReducer,
  orderDelete: orderDeleteReducer,
  subscriptionActive: subscriptionActiveReducer,
  subscriptionCreate: subscriptionCreateReducer,
  subscriptionDelete: subscriptionDeleteReducer,
  subscriptionDetails: subscriptionDetailsReducer,
  subscriptionList: subscriptionListReducer,
  mySubscriptionList: mySubscriptionListReducer,
  courseSubscription: courseSubscriptionReducer
});
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  reducer,
  initialState,
  composeEnhancer(applyMiddleware(thunk))
);
export default store;

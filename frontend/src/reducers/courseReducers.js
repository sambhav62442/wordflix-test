import {
  COURSE_LIST_REQUEST,
  COURSE_LIST_SUCCESS,
  COURSE_LIST_FAIL,
  COURSE_DETAILS_REQUEST,
  COURSE_DETAILS_SUCCESS,
  COURSE_DETAILS_FAIL,
  COURSE_SAVE_REQUEST,
  COURSE_SAVE_SUCCESS,
  COURSE_SAVE_FAIL,
  COURSE_DELETE_REQUEST,
  COURSE_DELETE_SUCCESS,
  COURSE_DELETE_FAIL,
  COURSE_REVIEW_SAVE_SUCCESS,
  COURSE_REVIEW_SAVE_REQUEST,
  COURSE_REVIEW_SAVE_FAIL,
  COURSE_REVIEW_SAVE_RESET,
  COURSE_VIDEO_SAVE_REQUEST,
  COURSE_VIDEO_SAVE_FAIL,
  COURSE_VIDEO_SAVE_SUCCESS,
  COURSE_VIDEO_DELETE_REQUEST,
  COURSE_VIDEO_DELETE_FAIL,
  COURSE_VIDEO_DELETE_SUCCESS,
} from '../constants/courseConstants';

function courseListReducer(state = { courses: [] }, action) {
  switch (action.type) {
    case COURSE_LIST_REQUEST:
      return { loading: true, courses: [] };
    case COURSE_LIST_SUCCESS:
      return { loading: false, courses: action.payload };
    case COURSE_LIST_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function courseDetailsReducer(state = { course: { reviews: [], videos: [] } }, action) {
  switch (action.type) {
    case COURSE_DETAILS_REQUEST:
      return { loading: true };
    case COURSE_DETAILS_SUCCESS:
      return { loading: false, course: action.payload };
    case COURSE_DETAILS_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function courseDeleteReducer(state = { course: {} }, action) {
  switch (action.type) {
    case COURSE_DELETE_REQUEST:
      return { loading: true };
    case COURSE_DELETE_SUCCESS:
      return { loading: false, course: action.payload, success: true };
    case COURSE_DELETE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function courseSaveReducer(state = { course: {} }, action) {
  switch (action.type) {
    case COURSE_SAVE_REQUEST:
      return { loading: true };
    case COURSE_SAVE_SUCCESS:
      return { loading: false, success: true, course: action.payload };
    case COURSE_SAVE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}
function courseReviewSaveReducer(state = {}, action) {
  switch (action.type) {
    case COURSE_REVIEW_SAVE_REQUEST:
      return { loading: true };
    case COURSE_REVIEW_SAVE_SUCCESS:
      return { loading: false, review: action.payload, success: true };
    case COURSE_REVIEW_SAVE_FAIL:
      return { loading: false, errror: action.payload };
    case COURSE_REVIEW_SAVE_RESET:
      return {};
    default:
      return state;
  }
}

function courseVideoSaveReducer(state = { course: {} }, action) {
  switch (action.type) {
    case COURSE_VIDEO_SAVE_REQUEST:
      return { loading: true };
    case COURSE_VIDEO_SAVE_SUCCESS:
      return { loading: false, success: true, course: action.payload };
    case COURSE_VIDEO_SAVE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function courseVideoDeleteReducer(state = { course: {} }, action) {
  switch (action.type) {
    case COURSE_VIDEO_DELETE_REQUEST:
      return { loading: true };
    case COURSE_VIDEO_DELETE_SUCCESS:
      return { loading: false, success: true, course: action.payload };
    case COURSE_VIDEO_DELETE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

export {
  courseListReducer,
  courseDetailsReducer,
  courseSaveReducer,
  courseDeleteReducer,
  courseReviewSaveReducer,
  courseVideoSaveReducer,
  courseVideoDeleteReducer
};

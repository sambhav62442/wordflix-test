import {
    SUBSCRIPTION_CREATE_REQUEST, SUBSCRIPTION_CREATE_SUCCESS, SUBSCRIPTION_CREATE_FAIL,
    SUBSCRIPTION_DETAILS_REQUEST, SUBSCRIPTION_DETAILS_SUCCESS, SUBSCRIPTION_DETAILS_FAIL,
    SUBSCRIPTION_ACTIVE_REQUEST, SUBSCRIPTION_ACTIVE_SUCCESS, SUBSCRIPTION_ACTIVE_FAIL,
    MY_SUBSCRIPTION_LIST_REQUEST, MY_SUBSCRIPTION_LIST_SUCCESS, MY_SUBSCRIPTION_LIST_FAIL,
    SUBSCRIPTION_LIST_REQUEST, SUBSCRIPTION_LIST_SUCCESS, SUBSCRIPTION_LIST_FAIL, SUBSCRIPTION_COURSE_REQUEST, SUBSCRIPTION_COURSE_SUCCESS, SUBSCRIPTION_COURSE_FAIL, SUBSCRIPTION_DELETE_REQUEST, SUBSCRIPTION_DELETE_SUCCESS, SUBSCRIPTION_DELETE_FAIL
  } from "../constants/subscriptionConstants";
  
  
  function subscriptionCreateReducer(state = {}, action) {
    switch (action.type) {
      case SUBSCRIPTION_CREATE_REQUEST:
        return { loading: true };
      case SUBSCRIPTION_CREATE_SUCCESS:
        return { loading: false, subscription: action.payload, success: true };
      case SUBSCRIPTION_CREATE_FAIL:
        return { loading: false, error: action.payload };
      default: return state;
    }
  }
  
  
  function subscriptionDetailsReducer(state = {
    subscription: {
      subscriptionItems: [],
      payment: {}
    }
  }, action) {
    switch (action.type) {
      case SUBSCRIPTION_DETAILS_REQUEST:
        return { loading: true };
      case SUBSCRIPTION_DETAILS_SUCCESS:
        return { loading: false, subscription: action.payload };
      case SUBSCRIPTION_DETAILS_FAIL:
        return { loading: false, error: action.payload };
      default: return state;
    }
  }
  
  
  function mySubscriptionListReducer(state = {
    subscriptions: []
  }, action) {
    switch (action.type) {
      case MY_SUBSCRIPTION_LIST_REQUEST:
        return { loading: true };
      case MY_SUBSCRIPTION_LIST_SUCCESS:
        return { loading: false, subscriptions: action.payload };
      case MY_SUBSCRIPTION_LIST_FAIL:
        return { loading: false, error: action.payload };
      default: return state;
    }
  }
  
  function courseSubscriptionReducer(state = {
    subscriptions: []
  }, action) {
    switch (action.type) {
      case SUBSCRIPTION_COURSE_REQUEST:
        return { loading: true };
      case SUBSCRIPTION_COURSE_SUCCESS:
        return { loading: false, subscription: action.payload };
      case SUBSCRIPTION_COURSE_FAIL:
        return { loading: false, error: action.payload };
      default: return state;
    }
  }

  function subscriptionListReducer(state = {
    subscriptions: []
  }, action) {
    switch (action.type) {
      case SUBSCRIPTION_LIST_REQUEST:
        return { loading: true };
      case SUBSCRIPTION_LIST_SUCCESS:
        return { loading: false, subscriptions: action.payload };
      case SUBSCRIPTION_LIST_FAIL:
        return { loading: false, error: action.payload };
      default: return state;
    }
  }
  
  function subscriptionActiveReducer(state = {
    subscription: {
      subscriptionItems: [],
      active: {}
    }
  }, action) {
    switch (action.type) {
      case SUBSCRIPTION_ACTIVE_REQUEST:
        return { loading: true };
      case SUBSCRIPTION_ACTIVE_SUCCESS:
        return { loading: false, success: true };
      case SUBSCRIPTION_ACTIVE_FAIL:
        return { loading: false, error: action.payload };
      default: return state;
    }
  }
  
  function subscriptionDeleteReducer(state = {
    subscription: {
      subscriptionItems: [],
      payment: {}
    }
  }, action) {
    switch (action.type) {
      case SUBSCRIPTION_DELETE_REQUEST:
        return { loading: true };
      case SUBSCRIPTION_DELETE_SUCCESS:
        return { loading: false, success: true };
      case SUBSCRIPTION_DELETE_FAIL:
        return { loading: false, error: action.payload };
      default: return state;
    }
  }
  export {
    subscriptionCreateReducer, 
    subscriptionDetailsReducer,
    subscriptionActiveReducer, 
    mySubscriptionListReducer, 
    courseSubscriptionReducer, 
    subscriptionListReducer, 
    subscriptionDeleteReducer
  }
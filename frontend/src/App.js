import React, {useState, useEffect} from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import './App.css';
import HomeScreen from './screens/HomeScreen';
import CourseScreen from './screens/CourseScreen';
import CartScreen from './screens/CartScreen';
import SigninScreen from './screens/SigninScreen';
import { useSelector } from 'react-redux';
import RegisterScreen from './screens/RegisterScreen';
import CoursesScreen from './screens/CoursesScreen';
import VideosScreen from './screens/VideosScreen';
import PaymentScreen from './screens/PaymentScreen';
import PlaceOrderScreen from './screens/PlaceOrderScreen';
import OrderScreen from './screens/OrderScreen';
import ProfileScreen from './screens/ProfileScreen';
import OrdersScreen from './screens/OrdersScreen';
import CourseVideoScreen from './screens/courseVideoScreen';
import ProfileIcon from './components/ProfileIcon';

function App() {
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;
  const [initials, setInitials] = useState('');

  useEffect(() => {
    if (userInfo) {
      setInitials(userInfo.name.split(" ").map((n)=>n[0]).join(""));
    }
    return () => {

    };
  }, [userInfo])

  return (
    <BrowserRouter>
      <div>
        <header>
          <nav className="navbar navbar-expand-lg navbar-light bg-white">
            <a className="navbar-brand" href="/">
            <svg className="svg-icon svg-icon-wordflix-logo" width="167px" height="45px" xmlns="http://www.w3.org/2000/svg" version="1.0" viewBox="0 0 1631.000000 384.000000" preserveAspectRatio="xMidYMid meet">
              <g transform="translate(0.000000,384.000000) scale(0.100000,-0.100000)" fill="#e50914" stroke="none">
                <path d="M3785 3819 c-290 -42 -505 -187 -615 -412 -80 -166 -82 -182 -90 -712 -12 -709 -11 -1539 0 -1635 35 -288 177 -490 408 -577 100 -37 203 -54 339 -54 594 -1 958 271 1013 756 8 69 10 397 8 1065 l-3 965 -28 88 c-33 107 -61 163 -120 243 -106 144 -300 243 -538 274 -89 11 -291 11 -374 -1z m267 -470 c95 -20 155 -63 192 -136 36 -72 38 -150 29 -1128 l-8 -950 -23 -51 c-44 -98 -125 -153 -256 -174 -140 -22 -249 23 -301 125 l-30 59 0 1005 c0 1123 -3 1073 70 1158 70 82 201 118 327 92z"></path>
                <path d="M7593 3822 l-203 -2 0 -1598 0 -1597 497 2 c274 2 542 8 596 14 346 37 560 193 645 470 41 131 43 219 39 1169 -3 821 -5 912 -21 980 -76 321 -289 504 -641 549 -84 11 -567 18 -912 13z m832 -473 c22 -7 61 -34 86 -59 84 -83 80 -39 77 -1095 -3 -1012 1 -947 -64 -1024 -60 -72 -156 -93 -399 -89 l-160 3 -3 1130 c-1 622 0 1136 3 1144 7 18 401 10 460 -10z"></path>
                <path d="M5454 3812 l-201 -2 -6 -878 c-4 -482 -7 -1216 -7 -1631 l0 -753 28 5 c15 3 145 11 290 18 l262 13 0 664 0 664 178 -4 c159 -3 182 -6 230 -27 31 -14 69 -42 92 -68 72 -82 73 -85 81 -583 6 -423 12 -501 40 -588 l11 -34 292 7 c160 4 292 9 294 10 1 1 -2 15 -8 31 -41 106 -43 130 -50 599 -6 426 -8 466 -28 538 -46 167 -135 284 -271 356 l-72 38 68 39 c97 56 174 139 223 237 67 135 75 190 74 512 0 312 -9 367 -76 505 -59 122 -199 234 -353 284 -152 48 -376 58 -1091 48z m748 -466 c71 -16 118 -48 152 -99 41 -64 48 -125 44 -400 l-3 -253 -30 -59 c-36 -70 -93 -117 -168 -140 -33 -11 -110 -18 -214 -22 l-163 -6 0 491 0 491 23 4 c55 11 306 6 359 -7z"></path>
                <path d="M9560 3334 c0 -269 3 -991 7 -1605 l6 -1117 176 -6 c97 -4 226 -9 286 -12 l110 -6 -3 673 -2 672 132 -6 c73 -4 244 -10 381 -13 l247 -7 0 230 0 230 -122 6 c-66 4 -169 7 -228 7 -59 0 -175 3 -259 7 l-151 6 2 481 3 481 280 -3 c154 -2 369 -5 478 -7 l197 -5 0 235 0 235 -267 0 c-148 0 -494 3 -770 7 l-503 6 0 -489z"></path>
                <path d="M11402 2163 l3 -1638 185 -12 c282 -18 714 -51 1025 -78 154 -14 291 -25 305 -25 l25 0 -3 239 -3 238 -62 7 c-34 3 -248 19 -476 36 -228 16 -415 31 -417 33 -2 1 -6 640 -10 1420 l-7 1417 -284 0 -283 0 2 -1637z"></path>
                <path d="M2548 3773 l-217 -3 -6 -33 c-6 -39 -233 -2081 -270 -2432 -15 -137 -30 -259 -35 -270 -8 -19 -9 -19 -9 -1 -1 26 -269 2621 -277 2675 l-6 44 -274 -5 c-152 -3 -278 -9 -281 -12 -5 -5 -159 -1513 -258 -2531 -15 -154 -29 -282 -30 -284 -8 -7 -35 234 -245 2169 -33 305 -62 574 -65 596 l-6 42 -172 -4 c-95 -3 -222 -7 -283 -10 l-110 -6 43 -386 c24 -213 109 -963 189 -1667 79 -704 152 -1349 161 -1433 19 -170 11 -159 104 -142 30 6 199 31 377 55 178 25 328 47 333 50 5 4 9 16 9 28 0 42 201 1832 208 1850 4 10 20 -100 36 -245 36 -337 164 -1490 171 -1543 l6 -41 52 7 c29 3 202 24 385 45 308 36 333 40 337 59 2 11 45 358 95 770 125 1034 319 2604 325 2638 4 26 3 27 -33 25 -20 -2 -135 -4 -254 -5z"></path>
                <path d="M13205 2081 c4 -933 8 -1697 9 -1697 0 -1 120 -14 266 -28 146 -15 273 -28 283 -29 16 -2 17 83 17 1713 0 943 -3 1717 -8 1721 -4 4 -135 9 -291 11 l-284 5 8 -1696z"></path>
                <path d="M14120 3754 c0 -6 50 -149 111 -319 211 -589 489 -1386 489 -1403 0 -17 -228 -642 -494 -1352 l-144 -385 31 -6 c53 -11 507 -63 511 -58 3 4 277 777 396 1119 40 113 75 208 79 213 4 4 26 -50 48 -120 227 -716 427 -1330 437 -1339 12 -11 585 -98 593 -90 7 7 -54 195 -364 1129 l-274 828 14 42 c247 701 587 1681 587 1692 0 1 -66 6 -147 9 -82 3 -202 9 -268 12 l-121 5 -18 -58 c-44 -139 -418 -1229 -424 -1235 -4 -4 -106 289 -227 652 l-220 660 -140 0 c-77 0 -211 3 -297 7 -97 4 -158 2 -158 -3z"></path>
              </g>
            </svg>
            </a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarText">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                  <a className="nav-link" href="/cart">Cart</a>
                </li>
                {userInfo ? (
                <li className="nav-item">
                  <a className="nav-link" href="/profile">
                    <ProfileIcon color="rgb(115, 114, 108)" fontSize="1em" value={initials} />
                  </a>
                </li>
                ) : (
                  <li className="nav-item">
                    <a className="nav-link" href="/signin">Sign In</a>
                  </li>
                )}
                {userInfo && userInfo.isAdmin && (
                  <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Admin
                    </a>
                    <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a className="dropdown-item" href="/orders">Orders</a>
                      <a className="dropdown-item" href="/courses">Courses</a>
                      <div className="dropdown-divider"></div>
                      <a className="dropdown-item" href="#">Something else here</a>
                    </div>
                  </li>
                )}
              </ul>
            </div>
          </nav>
        </header>
        <main className="main">
          <div className="content container">
            <Route path="/orders" component={OrdersScreen} />
            <Route path="/profile" component={ProfileScreen} />
            <Route path="/order/:id" component={OrderScreen} />
            <Route path="/courses" component={CoursesScreen} />
            <Route path="/course/:id/:videoId" component={CourseVideoScreen} />
            <Route path="/course/:id" exact={true} component={CourseScreen} />
            <Route path="/payment" component={PaymentScreen} />
            <Route path="/placeorder" component={PlaceOrderScreen} />
            <Route path="/signin" component={SigninScreen} />
            <Route path="/register" component={RegisterScreen} />
            <Route path="/videos" component={VideosScreen} />
            <Route path="/cart/:id?" component={CartScreen} />
            <Route path="/category/:id" component={HomeScreen} />
            <Route path="/" exact={true} component={HomeScreen} />
          </div>
        </main>
        <footer className="footer">© 2020 Wordflix. All right reserved.</footer>
      </div>
    </BrowserRouter>
  );
}

export default App;

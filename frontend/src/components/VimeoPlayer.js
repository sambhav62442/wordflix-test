import React, { useState, useEffect } from 'react';
function VimeoPlayer(props) {

  useEffect(() => {
    return () => {
      //
    };
  }, []);

  return !props.vimeoid ? (
    <div></div>
  ) : (
    <div className="vimeoLink" style={{position:"relative", paddingBottom:"56.25%", height:"0"}}>
        <iframe id="vimeo_player" src={`https://player.vimeo.com/video/${props.vimeoid}?color=e50914&byline=0&portrait=0`} style={{position:"absolute", top:0, left:0, width:"100%", height:"100%"}} frameBorder="0" allow="autoplay; fullscreen" autoPlay allowFullScreen></iframe>
    </div>
  );
}

export default VimeoPlayer;
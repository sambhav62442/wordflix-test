import React, { useEffect, useState } from 'react';

function VideoSearch(props) {
  const [searchTerm, setSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const courseVideos = props.courseVideos;

  useEffect(() => {
    if(!searchTerm.length == 0) {
      const results = courseVideos.filter(courseVideo =>
        courseVideo.name.toLowerCase().includes(searchTerm)
      );
      setSearchResults(results);
    } else {
      setSearchResults(courseVideos);
    }
  }, [searchTerm]);

  const handleSearch = event => {
    setSearchTerm(event.target.value);
  };

  const changeVideo = (coursevideoid) => {
    props.onVideoChange(coursevideoid);
  }

  return (
    <div>
      <h2>More Videos</h2>
      <input className="form-control" placeholder="Search" type="search" value={searchTerm} onChange={handleSearch}/>
        <ul className="more-videos" id="more-videos">
          {!searchResults.length && <li>There is no video</li>}
          {searchResults.map((coursevideo, i) => (
            <li className="videoItem" id={coursevideo._id} key={coursevideo._id} data-next={(i+1 < searchResults.length) ? searchResults[i+1]._id : ''} onClick={() => changeVideo(coursevideo._id)}>{coursevideo.name}</li>
          ))}
        </ul>
    </div>
  )
}

export default VideoSearch;
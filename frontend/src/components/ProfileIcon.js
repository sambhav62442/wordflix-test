import React from 'react';
function ProfileIcon(props) {
  return <div data-purpose="avatar" aria-hidden="true" className="udlite-avatar udlite-heading-md" style={{width:"2em", height:"2em", background: props.color, fontSize:props.fontSize}}>{props.value}</div>
}

export default ProfileIcon;